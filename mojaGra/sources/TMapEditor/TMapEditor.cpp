#include "TMapEditor.h"
#include <SFML/Graphics.hpp>
#include "sources/ResourcesManager/resourcesManager.h"
#include <fstream>
#include "sources/TMap/TMapDrower.h"
#include "sources/TMap/Map.h"
#include <iostream>

//==============================================
// Author: Piotr Magiera
//==============================================
TMapEditor::TMapEditor()
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapEditor::editor(sf::RenderWindow& renderWindow)
{
    sf::Vector2i MousePosition = sf::Mouse::getPosition( renderWindow );
    Map c;
    TMapDrower c1;

    bool Condition;
    unsigned int PositionOfBlockX=0;
    unsigned int PositionOfBlockY=0;

    for(int a=64; a<MousePosition.x;a+=64)
    {
        PositionOfBlockX=PositionOfBlockX+64;
    }

    for(int a=64; a<MousePosition.y;a+=64)
    {
        PositionOfBlockY=PositionOfBlockY+64;
    }

    c.insertField(PositionOfBlockX,PositionOfBlockY,NameOfBlock,m_fields,Condition);
    c1.drawer(renderWindow,m_fields,m_mapDrawers);

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) )
    {
        renderWindow.clear(sf::Color::Black);
        c.insertField(PositionOfBlockX,PositionOfBlockY,NameOfBlock,m_fields,Condition);

    }

    if(sf::Mouse::isButtonPressed(sf::Mouse::Right) )
    {
        c.DleteField(PositionOfBlockX,PositionOfBlockY,NameOfBlock,m_fields,Condition);
        renderWindow.clear(sf::Color::Black);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
    {
        for(unsigned int a=0;a<m_fields.size();a++)
            m_fields.pop_back();
    }

    if(m_fields.size()!=0&&Condition!=true)
        m_fields.pop_back();
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapEditor::MapLoad()
{
    Map c;
    c.loadMap(m_resourceManager);
    c.TMapLoader(m_fields);
    TMapDrower c1;
    c1.createDrawers(m_resourceManager,m_mapDrawers);
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapEditor::run()
{
    sf::RenderWindow renderWindow( sf::VideoMode( 1920 ,1080 , 32 ), "Editor Made by JA " );
    MapLoad();

    while( renderWindow.isOpen() )
    {
        renderWindow.clear(sf::Color::Black);
        processEvents(renderWindow);
        editor(renderWindow);
    }

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapEditor::processEvents(sf::RenderWindow& renderWindow)
{
     sf::Event event;
    while( renderWindow.pollEvent( event ) )
    {
        if( event.type == sf::Event::Closed )
             renderWindow.close();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            renderWindow.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            Map c;
            c.MapSeaving(m_fields);
        }

        if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num1)
            NameOfBlock=EResourceName::dirt;
        else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num2)
            NameOfBlock=EResourceName::gras;
        else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num3)
            NameOfBlock=EResourceName::wood;
        else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num4)
            NameOfBlock=EResourceName::leaf;
    }

}

