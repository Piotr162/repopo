#ifndef TMAPEDITOR_H
#define TMAPEDITOR_H
#include "sources/core/Board/TField.h"
#include "sources/drawer/Board/TFieldDrawer.h"
#include <SFML/Graphics/Texture.hpp>
#include <memory>

namespace sf
{
    class RenderWindow;
}


class TMapEditor
{
public:
    TMapEditor();
    void editor(sf::RenderWindow& renderWindow);
    void MapLoad();
    void processEvents(sf::RenderWindow &renderWindow);
    void run();

    TresourcesManager m_resourceManager;
    EResourceName NameOfBlock =EResourceName::dirt;
    std::vector<std::shared_ptr<TField>> m_fields;
    std::map<EResourceName, std::shared_ptr<TFieldDrawer>> m_mapDrawers;
};

#endif // TMAPEDITOR_H
