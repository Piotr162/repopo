#include "resourcesManager.h"
#include <string>

//==============================================
// Author: Piotr Magiera
//==============================================
TresourcesManager::TresourcesManager()
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TresourcesManager::ResourcesMenagerLoad(const std::string& textureLocal, EResourceName nameOfBlock)
{
    sf::Texture texture;
    texture.loadFromFile(textureLocal);
    resource[nameOfBlock] = texture;

}

//==============================================
// Author: Piotr Magiera
//==============================================
bool TresourcesManager::ResourcesMenagerget(sf::Texture &Mtexture, EResourceName nameOfBlock)
{
auto search = resource.find(nameOfBlock);
    if(search != resource.end())
    {
        Mtexture = resource[nameOfBlock];
        return  true;
    }
    return false;
}


