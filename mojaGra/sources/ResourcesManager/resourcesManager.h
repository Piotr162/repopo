#ifndef RESOURCESMANAGER_H
#define RESOURCESMANAGER_H
#include <map>
#include<SFML/Graphics/Texture.hpp>

enum class EResourceName
{
    stone,
    cobblestone,
    wood,
    dirt,
    gras,
    leaf
};


class TresourcesManager
{
public:
    TresourcesManager();

    void ResourcesMenagerLoad(const std::string &textureLocal, EResourceName nameOfBlock);
    bool ResourcesMenagerget( sf::Texture &Mtexture , EResourceName nameOfBlock);

private:
    std::map<EResourceName , sf::Texture > resource;

};

#endif // RESOURCESMANAGER_H
