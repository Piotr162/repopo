#ifndef TMAPDROWER_H
#define TMAPDROWER_H
#include <memory>
#include "sources/drawer/Board/TFieldDrawer.h"
#include "sources/entitis/Player/TPlayer.h"

namespace sf
{
    class RenderWindow;
}

class TMapDrower
{
public:
    TMapDrower();
    void createDrawers(TresourcesManager &m_resourceManager, std::map<EResourceName, std::shared_ptr<TFieldDrawer>> &m_mapDrawers);

    void drawer(sf::RenderWindow &renderWindow, std::vector<std::shared_ptr<TField> > m_fields, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > m_mapDrawers);
    void mapDrawer(sf::RenderWindow &renderWindow,std::vector<std::shared_ptr<TField>> m_fields,std::map<EResourceName, std::shared_ptr<TFieldDrawer>> m_mapDrawers);
    void drawerWitchPlayer(sf::RenderWindow &renderWindow, std::vector<std::shared_ptr<TField> > m_fields, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > m_mapDrawers,TPlayer c_player);
};

#endif // TMAPDROWER_H
