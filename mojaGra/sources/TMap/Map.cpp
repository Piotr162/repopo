#include "Map.h"
#include <fstream>

//==============================================
// Author: Piotr Magiera
//==============================================
Map::Map()
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
void Map::TMapLoader(std::vector<std::shared_ptr<TField>>& m_fields)
{
    std::fstream map;
    map.open( "Resources/Board/map", std::ios::in | std::ios::out);

    if( map.good() == true )
    {
        std::string Inside;
        std::string g;
        float x=0;
        float y=0;
        EResourceName s=EResourceName::dirt;
        unsigned int conditionOfOrder=0;
        getline( map,Inside );

        for (unsigned int a=0;a<Inside.length();a++)
        {
            char SingleLetterOfString;
            SingleLetterOfString=Inside.at(a);

            if(SingleLetterOfString==' ')
            {
                if(conditionOfOrder==0)
                {
                    x=std::stof(g);
                    conditionOfOrder=1;
                }
                else if(conditionOfOrder==1)
                {
                    y=std::stof(g);
                    conditionOfOrder=2;
                }
                else if(conditionOfOrder==2)
                {
                    if (g=="dirt")
                        s=EResourceName::dirt;
                    if (g=="gras")
                        s=EResourceName::gras;
                    if (g=="wood")
                        s=EResourceName::wood;
                    if (g=="leaf")
                        s=EResourceName::leaf;

                    conditionOfOrder=0;
                    bool Condition;
                    insertField(x,y,s,m_fields,Condition);
                }
                g.clear();
            }
            else
                g.push_back(SingleLetterOfString);

        }
        map.close();
    }
}

//==============================================
// Author: Piotr Magiera
//==============================================
void Map::insertField(float x, float y, EResourceName a, std::vector<std::shared_ptr<TField> > &m_fields,bool& Condition)
{
    Condition=false;
    for(unsigned int b=0;b<m_fields.size();b++)
    {
        int old_x = static_cast<int>(m_fields.at(b)->x());
        int old_y = static_cast<int>(m_fields.at(b)->y());
        int new_x = static_cast<int>(x);
        int new_y = static_cast<int>(y);
        if(old_x==new_x && old_y==new_y)
            Condition=true;
    }

    if(Condition!=true)
        m_fields.push_back(std::make_shared<TField>(x,y,a));
}

//==============================================
// Author: Piotr Magiera
//==============================================
void Map::DleteField(float x, float y, EResourceName a, std::vector<std::shared_ptr<TField> > &m_fields,bool& Condition)
{
    Condition=false;
    for(unsigned int b=0;b<m_fields.size();b++)
    {
        int old_x = static_cast<int>(m_fields.at(b)->x());
        int old_y = static_cast<int>(m_fields.at(b)->y());
        int new_x = static_cast<int>(x);
        int new_y = static_cast<int>(y);
        if(old_x==new_x && old_y==new_y)
            Condition=true;
    }

    if(Condition!=true)
        m_fields.push_back(std::make_shared<TField>(x,y,a));

    for(unsigned int b=0;b<m_fields.size();b++)
    {
        int old_x = static_cast<int>(m_fields.at(b)->x());
        int old_y = static_cast<int>(m_fields.at(b)->y());
        int new_x = static_cast<int>(x);
        int new_y = static_cast<int>(y);
        if(old_x==new_x && old_y==new_y)
        {
            m_fields.erase(m_fields.begin()+b);
        }
    }
}

//==============================================
// Author: Piotr Magiera
//==============================================
void Map::MapSeaving(std::vector<std::shared_ptr<TField> > &m_fields)
{
    std::fstream map;

    std::remove("Resources/Board/map");
    map.open( "Resources/Board/map", std::ios::out);

    if( map.good() == true )
    {

        for(unsigned int a=0;a<m_fields.size();a++)
        {
            if(a!=0)
                map<<" ";

            map<<m_fields.at(a)->x();
            map<<" ";
            map<<m_fields.at(a)->y();
            map<<" ";

            if(m_fields.at(a)->Resource==EResourceName::dirt)
                map<<"dirt";
            else if(m_fields.at(a)->Resource==EResourceName::gras)
                map<<"gras";
            else if(m_fields.at(a)->Resource==EResourceName::wood)
                map<<"wood";
            else if(m_fields.at(a)->Resource==EResourceName::leaf)
                map<<"leaf";
        }

        map.close();
    }
}

//==============================================
// Author: Piotr Magiera
//==============================================
void Map::loadMap(TresourcesManager &m_resourceManager)
{
    m_resourceManager.ResourcesMenagerLoad("Resources/Board/dirt.png", EResourceName::dirt);
    m_resourceManager.ResourcesMenagerLoad("Resources/Board/gras.png", EResourceName::gras);
    m_resourceManager.ResourcesMenagerLoad("Resources/Board/wood.png", EResourceName::wood);
    m_resourceManager.ResourcesMenagerLoad("Resources/Board/leaf.png", EResourceName::leaf);
}
