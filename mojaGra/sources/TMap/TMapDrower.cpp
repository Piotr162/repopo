#include "TMapDrower.h"
#include <SFML/Graphics.hpp>
#include "sources/core/Board/TField.h"
#include "sources/entitis/Player/TPlayer.h"

//==============================================
// Author: Piotr Magiera
//==============================================
TMapDrower::TMapDrower()
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapDrower::createDrawers(TresourcesManager &m_resourceManager, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > &m_mapDrawers)
{
    m_mapDrawers[EResourceName::dirt] = std::make_shared<TFieldDrawer>(EResourceName::dirt, m_resourceManager);
    m_mapDrawers[EResourceName::gras] = std::make_shared<TFieldDrawer>(EResourceName::gras, m_resourceManager);
    m_mapDrawers[EResourceName::wood] = std::make_shared<TFieldDrawer>(EResourceName::wood, m_resourceManager);
    m_mapDrawers[EResourceName::leaf] = std::make_shared<TFieldDrawer>(EResourceName::leaf, m_resourceManager);

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TMapDrower::drawer(sf::RenderWindow& renderWindow, std::vector<std::shared_ptr<TField> > m_fields, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > m_mapDrawers)
{
    mapDrawer(renderWindow,m_fields,m_mapDrawers);
    renderWindow.display();
}

void TMapDrower::drawerWitchPlayer(sf::RenderWindow& renderWindow, std::vector<std::shared_ptr<TField> > m_fields, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > m_mapDrawers,TPlayer player)
{

    mapDrawer(renderWindow,m_fields,m_mapDrawers);
    sf::Sprite sprite;
    player.draw(renderWindow,sprite);
    renderWindow.display();
}
//==============================================
// Author: Piotr Magiera
//==============================================
void TMapDrower::mapDrawer(sf::RenderWindow& renderWindow, std::vector<std::shared_ptr<TField> > m_fields, std::map<EResourceName, std::shared_ptr<TFieldDrawer> > m_mapDrawers)
{
    for(const auto& field : m_fields)
    {
        m_mapDrawers[field->Resource]->draw(*field, renderWindow);
    }
}
