#ifndef MAP_H
#define MAP_H
#include <vector>
#include <memory>
#include "sources/core/Board/TField.h"


class Map
{
public:
    Map();
    void TMapLoader(std::vector<std::shared_ptr<TField> > &m_fields);
    void insertField(float x, float y, EResourceName a, std::vector<std::shared_ptr<TField> > &m_fields, bool& Condition);
    void loadMap(TresourcesManager &m_resourceManager);
    void DleteField(float x, float y, EResourceName a, std::vector<std::shared_ptr<TField> > &m_fields,bool& Condition);
    void MapSeaving(std::vector<std::shared_ptr<TField> > &m_fields);
};

#endif // MAP_H
