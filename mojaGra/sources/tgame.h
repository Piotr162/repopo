#ifndef TGAME_H
#define TGAME_H
#include "core/Board/TField.h"
#include "drawer/Board/TFieldDrawer.h"
#include <memory>
#include "sources/entitis/Player/TPlayer.h"

namespace sf
{
    class RenderWindow;
}

class TGame
{
public:
    TGame();
    void run();

private:
    void processEvents(sf::RenderWindow& renderWindow, TPlayer &player);
    void updateLogic();

    //  TPlayer player;

    TresourcesManager m_resourceManager;

    std::vector<std::shared_ptr<TField>> m_fields;
    std::map<EResourceName, std::shared_ptr<TFieldDrawer>> m_mapDrawers;

};

#endif // TGAME_H
