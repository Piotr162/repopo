#include <SFML/Graphics.hpp>
#include "tgame.h"
#include "ResourcesManager/resourcesManager.h"
#include "sources/TMap/TMapDrower.h"
#include "sources/TMapEditor/TMapEditor.h"
#include "sources/TMap/Map.h"
#include "sources/entitis/Player/TPlayer.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>

//==============================================
// Author: Piotr Magiera
//==============================================
TGame::TGame()
{
    Map c_Map;
    c_Map.loadMap(m_resourceManager);
    c_Map.TMapLoader(m_fields);
    TMapDrower c_MapDrawer;
    c_MapDrawer.createDrawers(m_resourceManager,m_mapDrawers);

}

//==============================================
// Author: Piotr Magiera
//==============================================
void TGame::run()
{
    sf::RenderWindow renderWindow( sf::VideoMode( 1920 ,1080 , 32 ), "Gra Made by JA " );
    renderWindow.clear(sf::Color::Black);
    TPlayer p;
    TMapDrower c_MapDrawer;
    p.texture();
    while( renderWindow.isOpen() )
    {
            usleep(100000);
        p.Update(1);
        renderWindow.clear(sf::Color::Black);
        processEvents(renderWindow,p);

        updateLogic();
        c_MapDrawer.drawerWitchPlayer(renderWindow,m_fields,m_mapDrawers,p);

    }
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TGame::processEvents(sf::RenderWindow& renderWindow,TPlayer&player)
{
     sf::Event Event;
    while( renderWindow.pollEvent( Event ) )
    {
        if( Event.type == sf::Event::Closed )
             renderWindow.close();

        else if( Event.type == sf::Event::KeyPressed && Event.key.code == sf::Keyboard::Escape )
             renderWindow.close();

        else if (Event.type == sf::Event::KeyPressed&&Event.key.code == sf::Keyboard::Space)
        {
            player.Jump();
        }
        else if (Event.type == sf::Event::KeyPressed&&Event.key.code == sf::Keyboard::A)
        {
           player.GoLeft();
        }
        else if (Event.type == sf::Event::KeyPressed&&Event.key.code == sf::Keyboard::D)
        {
            player.GoRight();
        }
        else if (Event.type == sf::Event::KeyPressed&&Event.key.code == sf::Keyboard::LShift)
        {
            player.Run();
        }
        else if (Event.type == sf::Event::KeyReleased&&Event.key.code == sf::Keyboard::A)
        {
            player.StopLeft();
        }
        else if (Event.type == sf::Event::KeyReleased&&Event.key.code == sf::Keyboard::D)
        {
            player.StopRight();
        }
        else if (Event.type == sf::Event::KeyReleased&&Event.key.code == sf::Keyboard::LShift)
        {
            player.StopRunning();
        }
        else if (Event.type == sf::Event::KeyReleased&&Event.key.code == sf::Keyboard::Q)
        {
            player.clear();
        }
    }
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TGame::updateLogic()
{

}
