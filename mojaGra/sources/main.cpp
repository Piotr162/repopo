#include <iostream>
#include "tgame.h"
#include "TMapEditor/TMapEditor.h"

int main()
{
    int d = 0;
    std::cout << "Choose Mode: 0 - Game; Other case MapEditor" << std::endl;
    std::cin>>d;
    if(d==0)
    {
        TGame c;
        c.run();
    }
    else
    {
        TMapEditor c;
        c.run();
    }

    return 0;
}
