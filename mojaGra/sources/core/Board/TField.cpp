#include "TField.h"

//==============================================
// Author: Piotr Magiera
//==============================================
TField::TField() :
    m_x(),
    m_y()
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
TField::TField(float A_x, float A_y, EResourceName A_name) :
    Resource(A_name),
    m_x(A_x),
    m_y(A_y)
{

}

//==============================================
// Author: Piotr Magiera
//==============================================
float TField::x() const
{
    return m_x;
}

//==============================================
// Author: Piotr Magiera
//==============================================
float TField::y() const
{
    return m_y;
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TField::setX(float xx)
{
    m_x=xx;
}

//==============================================
// Author: Piotr Magiera
//==============================================
void TField::setY(float yy)
{
    m_y=yy;
}

