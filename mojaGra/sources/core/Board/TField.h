#ifndef TFIELD_H
#define TFIELD_H
#include "../../ResourcesManager/resourcesManager.h"

class TField
{
public:
    TField();
    TField(float A_x, float A_y, EResourceName A_name);

    float x() const;
    float y() const;
    void setX(float xx);
    void setY(float yy);

    EResourceName Resource;
protected:
    float m_x;
    float m_y;
};

#endif // TFIELD_H
