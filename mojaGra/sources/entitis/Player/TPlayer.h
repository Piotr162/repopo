#ifndef TPLAYER_H
#define TPLAYER_H
#include <SFML/Graphics.hpp>
namespace sf
{
    class RenderWindow;
}

class TPlayer
{
public:
    TPlayer();
    void Update(double dt);
    double GetX() const { return m_x; }
    double GetY() const { return m_y; }
    double GetDefaultYVelocity()     const { return DefaultYVelocity; }
    double GetDefaultYAcceleration() const { return DefaultYAcceleration; }

    void Run()               { m_running_factor = 2.0; }
    void StopRunning()       { m_running_factor = 1.0; }
    void GoLeft()            { m_vx -= 4.0;}
    void GoRight()           { m_vx += 4.0;}
    void StopLeft()          { m_vx += 4.0;}
    void StopRight()         { m_vx -= 4.0;}
    void ForbidGoingLeft()   { m_can_go_left = false; }
    void ForbidGoingRight()  { m_can_go_right = false; }
    void Fall()              { m_vy = 0.0; m_is_on_ground = false; }
    void Jump(double y_velocity = DefaultYVelocity);
    void AllowToJump()       { m_jump_allowed = true; }
    void ForbidToJump()      { m_jump_allowed = false; }
    void SetDefaultMoving()  { m_is_on_ground = false; m_can_go_right = m_can_go_left = true;}
    void PlayerOnGround() {
        m_is_on_ground = m_jump_allowed = true;
        m_vy = 0;
    }
    void clear();

    double GetNextXPosition(double dt) const { return m_x + m_vx * dt * m_running_factor; }
    double GetNextYPosition(double dt) const { return m_y + (m_vy + m_ay * dt) * dt; }
    void event_movment(sf::Event event);
    void texture();
    void draw(sf::RenderWindow& renderWindow, sf::Sprite sprite);
    int f=0;

private:
    enum { DefaultYVelocity =20, DefaultYAcceleration=-60 };
    double m_x;               // położenie postaci na osi odciętych
    double m_y;               // położenie postaci na osi rzędnych
    double m_vx;              // prędkość na osi OX
    double m_vy;              // prędkość gracza w pionie
    double m_ay;              // przyspieszenie gracza w pionie
    double m_running_factor;  // współczynnik biegania.
                              //         >1.0 => biegnie,
                              //         <1.0 => spowolnienie
    bool m_jump_allowed;      // czy gracz może skakać
                              // (np. jest na podłożu)
    //size_t m_level_width;     // szerokość poziomu (w kaflach)
    bool m_is_on_ground;      // czy postać jest na podłożu
    bool m_can_go_left;       // czy postać może iść w lewo
    bool m_can_go_right;      // czy postać może iść w prawo
    sf::Texture ttexture;
    double gravity=68;


};

#endif // TPLAYER_H
