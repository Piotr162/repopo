#include "TPlayer.h"
#include <SFML/Graphics.hpp>

TPlayer::TPlayer()
    :
      m_x(5),
      m_y(50),
      m_vx(0.0),
      m_vy(0),
      m_ay(DefaultYAcceleration),
      m_running_factor(1.0),
      m_jump_allowed(true),
      //m_level_width(level_width),
      m_is_on_ground(true),
      m_can_go_left(true),
      m_can_go_right(true) {
    SetDefaultMoving();
}


void TPlayer::Jump(double y_velocity) {
    // wykonaj skok o ile jest taka możliwość
    if (m_jump_allowed) {
        m_jump_allowed = false;
        m_is_on_ground = false;
        m_vy = y_velocity;            // początkowa prędkość
        m_ay = DefaultYAcceleration;  // przyspieszenie
    }
}

void TPlayer::clear()
{
    m_vx=0;

}

void TPlayer::texture()
{

    ttexture.loadFromFile("/home/piotr123/Documents/prjocts_Qt/MYGAME/mojaGra/Resources/Board/player.png");

}

void TPlayer::Update(double dt) {
    // wylicz nową prędkość oraz połóżenie na osi OY
    if (!m_is_on_ground)
    {
        m_y = GetNextYPosition(dt);
        m_vy += m_ay * dt;
        m_vy+=gravity;
    }

    // jeżeli poniżej pierwszego kafla, to nie spadaj niżej.
    // Na razie ustalamy poziom na y=1, aby postać nie uciekała za ekran
    if(f==0)
    {
        f++;
        if (m_y < 830) {
            m_y = 830;
            PlayerOnGround();
        }
    }else
    {
        if (m_y > 830) {
            m_y = 830;
            PlayerOnGround();
        }
    }

    // wylicz pozycję gracza w poziomie (oś OX).
    double next_x = GetNextXPosition(dt);
    if (next_x < m_x && m_can_go_left) {
        m_x = next_x;
    } else if (next_x > m_x && m_can_go_right) {
        m_x = next_x;
    }

    // nie można wyjść poza mapę
    if (m_x < 0) {
        m_x = 0; // nie można wyjść za początek mapy
    } else if (m_x > 1880 - 1) {
        m_x = 1880 - 1; // nie można wyjść za ostatni kafel mapy
    }
    //m_vx=0;
}




void TPlayer::draw(sf::RenderWindow &renderWindow,sf::Sprite sprite)
{
    sprite.setTexture(ttexture);
    float xx=static_cast<float>(m_x);
    float yy=static_cast<float>(m_y);
    sprite.setPosition(xx,yy);
    renderWindow.draw(sprite);
}

