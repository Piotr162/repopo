#ifndef TFIELDDROWER_H
#define TFIELDDROWER_H
#include <SFML/Graphics/Sprite.hpp>
#include "IFieldDrawer.h"
#include "../../ResourcesManager/resourcesManager.h"

class TField;

namespace sf
{

    class Texture;
}

class TFieldDrawer : public IFieldDrawer
{
public:
    TFieldDrawer(EResourceName name, TresourcesManager& manager);
    void draw(const TField &fieldData, sf::RenderWindow& renderWindow) override;

protected:
    EResourceName   m_resourceName;
    sf::Texture     m_texture;
    sf::Sprite      m_sprite;
};

#endif // TFIELDDROWER_H
