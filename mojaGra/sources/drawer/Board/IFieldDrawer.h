#pragma once

namespace sf
{
    class RenderWindow;
}

class TField;

class IFieldDrawer
{
  public:
    virtual void draw(const TField &fieldData, sf::RenderWindow& renderWindow) = 0;
    virtual ~IFieldDrawer()=default;
};


