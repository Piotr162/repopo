#include "TFieldDrawer.h"
#include "../../core/Board/TField.h"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <string>

//==============================================
// Author: Piotr Magiera
//==============================================
TFieldDrawer::TFieldDrawer(EResourceName name, TresourcesManager& manager)
{
    manager.ResourcesMenagerget(m_texture,name);
    m_sprite.setTexture(m_texture);

}
//==============================================
// Author: Piotr Magiera
//==============================================
void TFieldDrawer::draw(const TField& fieldData, sf::RenderWindow &renderWindow)
{

    m_sprite.setPosition(fieldData.x(), fieldData.y());
    renderWindow.draw( m_sprite );
}
