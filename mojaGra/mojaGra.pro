QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        sources/ResourcesManager/resourcesManager.cpp \
        sources/TMap/Map.cpp \
        sources/TMap/TMapDrower.cpp \
        sources/TMapEditor/TMapEditor.cpp \
        sources/core/Board/TField.cpp \
        sources/drawer/Board/TFieldDrawer.cpp \
        sources/entitis/Player/TPlayer.cpp \
        sources/main.cpp \
        sources/tgame.cpp
LIBS += -L"../externals/SFML-2.5.1/lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system

INCLUDEPATH += "../externals/SFML-2.5.1/include"
DEPENDPATH += "../externals/SFML-2.5.1/include"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    sources/ResourcesManager/resourcesManager.h \
    sources/TMap/Map.h \
    sources/TMap/TMapDrower.h \
    sources/TMapEditor/TMapEditor.h \
    sources/core/Board/TField.h \
    sources/drawer/Board/IFieldDrawer.h \
    sources/drawer/Board/TFieldDrawer.h \
    sources/entitis/Player/TPlayer.h \
    sources/tgame.h
